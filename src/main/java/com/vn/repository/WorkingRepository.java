package com.vn.repository;

import com.vn.entities.Working;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingRepository extends JpaRepository<Working, Integer> {
}
