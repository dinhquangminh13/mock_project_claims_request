package com.vn.repository;

import com.vn.entities.ClaimsRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimsRequestRepository extends JpaRepository<ClaimsRequest, Integer> {
}
