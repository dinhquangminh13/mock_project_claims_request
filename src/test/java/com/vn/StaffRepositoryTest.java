package com.vn;

import com.vn.entities.Staff;
import com.vn.entities.Status;
import com.vn.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StaffRepositoryTest {

    @Autowired
    StaffRepository staffRepository;

    @Test
    void StaffRepositoryTest_TC001_success() {
        Staff staff = new Staff();
        staff.setName("Huy Gau 01");
        staff.setDepartment("FSA");
        staff.setRank("Trainer");
        staff.setSalary(50000.00d);
        staff.setPassword("huy01");
        staff.setRePassword("huy01");
        staff.setEmail("huypn2@fpt.com");
        staff.setStatus(Status.STATUS_UNALLOCATED);
        staff.setRole(null);
        staffRepository.save(staff);

    }
}
