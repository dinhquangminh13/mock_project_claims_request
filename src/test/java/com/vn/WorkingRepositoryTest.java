package com.vn;

import com.vn.entities.Project;
import com.vn.entities.Staff;
import com.vn.entities.Working;
import com.vn.repository.ProjectRepository;
import com.vn.repository.StaffRepository;
import com.vn.repository.WorkingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;

@SpringBootTest
public class WorkingRepositoryTest {

    @Autowired
    WorkingRepository workingRepository;

    @Autowired
    StaffRepository staffRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Test
    void WorkingRepository_TC001_success() throws ParseException {
        Working working = new Working();
        Staff staff = staffRepository.findById(4).orElse(null);
        Project project = projectRepository.findById(1).orElse(null);
        working.setStaff(staff);
        working.setProject(project);
        workingRepository.save(working);

    }
}
